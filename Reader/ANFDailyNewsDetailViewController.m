//
//  ANFDailyNewsDetailViewController.m
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import "ANFDailyNewsDetailViewController.h"
#import "UIImageView+AFNetworking.h"

@interface ANFDailyNewsDetailViewController()

@property (nonatomic, strong) IBOutlet UIWebView *contentWebView;
@property (nonatomic, strong) IBOutlet UILabel *tileLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

@implementation ANFDailyNewsDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.newsObject != nil)
    {
        
        // Add to Webview Title and Image
        self.tileLabel.text = self.newsObject.title;
        
        [self.contentWebView loadHTMLString:self.newsObject.content baseURL:[NSURL URLWithString:self.newsObject.link]];
        self.contentWebView.userInteractionEnabled = NO;
        
        
        NSURL *url = [NSURL URLWithString:self.newsObject.imageLink];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        
        __weak ANFDailyNewsDetailViewController *weakSelf = self;
        self.imageView.image = nil;
        UIImage *placeHolderImage = [UIImage imageNamed:@"placeholder-1"];
        [self.imageView setImageWithURLRequest:request placeholderImage:placeHolderImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             weakSelf.imageView.image = image;
             weakSelf.imageView.contentMode = UIViewContentModeScaleAspectFit;
             
             
         } failure:nil];
    }
}

@end
