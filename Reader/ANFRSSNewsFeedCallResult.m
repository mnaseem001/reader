//
//  ANFRSSNewsFeedCallResult.m
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import "ANFRSSNewsFeedCallResult.h"

@implementation ANFRSSNewsFeedCallResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"dailyNewsList":@"responseData.feed.entries",
    };
}

+ (NSValueTransformer *)dailyNewsListJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:ANFNewsObject.class];
}

@end
