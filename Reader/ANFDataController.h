//
//  ANFDataController.h
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANFDataController : NSObject

/*
 * Returns Singleton for ANFDataController.
 */
+ (id)sharedInstance;

- (void)fetchDailyNewsListWithCompletionHandler:(void(^)(NSArray *dailyNewsList, NSError *error))completion;

@end
