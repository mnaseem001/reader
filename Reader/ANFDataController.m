//
//  ANFDataController.m
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import "ANFDataController.h"
#import "ANFRSSNewsFeedCallResult.h"
#import "ANFNewsObject.h"

#define kRSSNewsFeedURL @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&q=http%3A%2F%2Fnews.google.com%2Fnews%3Foutput%3Drss"

@interface ANFDataController() <NSURLSessionDataDelegate>

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSArray *cachedDailyNewsList;

@end

@implementation ANFDataController

+ (id)sharedInstance {
    static ANFDataController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}

- (void)fetchDailyNewsListWithCompletionHandler:(void(^)(NSArray *dailyNewsList, NSError *error))completion
{
    NSURL *url = [NSURL URLWithString:kRSSNewsFeedURL];
    
    [self fetchJSONFromURL:url completionHandler:^(id json, NSError *error) {
        if (!error)
        {
            ANFRSSNewsFeedCallResult *result = [MTLJSONAdapter modelOfClass:[ANFRSSNewsFeedCallResult class] fromJSONDictionary:json error:nil];
            if (result)
            {
                self.cachedDailyNewsList = result.dailyNewsList;
                completion(result.dailyNewsList,nil);

            }
            else
            {
                if (self.cachedDailyNewsList != nil)
                {
                    completion(self.cachedDailyNewsList,nil);
                }
                else
                {
                    //get local copy
                    [self fetchLocalDataWithCompletionHandler:^(id json, NSError *error) {
                        ANFRSSNewsFeedCallResult *result = [MTLJSONAdapter modelOfClass:[ANFRSSNewsFeedCallResult class] fromJSONDictionary:json error:nil];
                        completion(result.dailyNewsList,nil);
                        
                    }];
                }
                
            }
            
        }
        else
        {
            if (self.cachedDailyNewsList != nil)
            {
                completion(self.cachedDailyNewsList,error);
            }
            else
            {
                //get local copy
                [self fetchLocalDataWithCompletionHandler:^(id json, NSError *error) {
                    ANFRSSNewsFeedCallResult *result = [MTLJSONAdapter modelOfClass:[ANFRSSNewsFeedCallResult class] fromJSONDictionary:json error:nil];
                    completion(result.dailyNewsList,nil);
                    
                }];
            }
        }
    }];
}


#pragma mark private methods

- (void)fetchJSONFromURL:(NSURL *)url completionHandler:(void(^)(id json, NSError *error))completion
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (!error)
        {
            NSError *jsonError = nil;
            
            id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            if (!jsonError)
            {
                completion(json,nil);
            }
            else
            {
                completion(nil,jsonError);
            }
        }
        else
        {
            completion(nil,error);
        }
    }];
    
    [dataTask resume];
}

- (void)fetchLocalDataWithCompletionHandler:(void(^)(id json, NSError *error))completion
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"reader_local_copy" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSError *jsonError = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
    completion(json, nil);
}

#pragma mark - private setters and getters
- (NSURLSession *)session
{
    if (_session == nil)
    {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    return _session;
}

@end
