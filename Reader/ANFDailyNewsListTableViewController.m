//
//  ANFDailyNewsListTableViewController.m
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import "ANFDailyNewsListTableViewController.h"
#import "ANFDailyNewsDetailViewController.h"
#import "ANFDailyNewsTableViewCell.h"
#import "ANFDataController.h"
#import "ANFNewsObject.h"
#import "UIImageView+AFNetworking.h"

@interface ANFDailyNewsListTableViewController ()

@property (nonatomic, strong) NSArray *dailyNewsList;

@end

@implementation ANFDailyNewsListTableViewController

static NSString *CellIdentifier = @"DailyNewsCell";

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[ANFDataController sharedInstance] fetchDailyNewsListWithCompletionHandler:^(NSArray *dailyNewsList, NSError *error) {
        
        if (!error) {
            self.dailyNewsList = dailyNewsList;
        }
        else
        {
            // Alert user or table view for any errors?
            // most likely returned caches result.
            self.dailyNewsList = dailyNewsList;
        }
        
        [self performSelectorOnMainThread:@selector(reloadDailyNewsList) withObject:nil waitUntilDone:NO];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    // Return the number of rows in the section.
    return [self.dailyNewsList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ANFDailyNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    ANFNewsObject *newsObject = (ANFNewsObject *)self.dailyNewsList[indexPath.row];
    cell.newsStoryTitleLabel.text = newsObject.title;
    [cell.newsStorySnippetWebView loadHTMLString:newsObject.contentSnippet baseURL:nil];
    cell.newsStorySnippetWebView.userInteractionEnabled = NO;
    
    NSURL *url = [NSURL URLWithString:newsObject.imageLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    __weak ANFDailyNewsTableViewCell *weakCell = cell;
    cell.newsStoryImageView.image = nil;
    UIImage *placeHolderImage = [UIImage imageNamed:@"placeholder-1"];
    [cell.newsStoryImageView setImageWithURLRequest:request placeholderImage:placeHolderImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
    {
                                            
        weakCell.newsStoryImageView.image = image;
        weakCell.newsStoryImageView.contentMode = UIViewContentModeScaleAspectFit;
                                            
                                            
    } failure:nil];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ANFNewsObject *newsObject = (ANFNewsObject *)self.dailyNewsList[indexPath.row];
    
    if (newsObject != nil)
    {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ANFDailyNewsDetailViewController *dailyNewsDetailViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"ANFDailyNewsDetailViewController"];
        [dailyNewsDetailViewController setNewsObject:newsObject];
        [self.navigationController pushViewController:dailyNewsDetailViewController animated:YES];
    }
}

#pragma mark - private methods

- (void)reloadDailyNewsList
{
    [self.tableView reloadData];
}

@end
