//
//  ANFNewsObject.h
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>

@interface ANFNewsObject : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *content;
@property (nonatomic, copy, readonly) NSString *contentSnippet;
@property (nonatomic, copy, readonly) NSString *imageLink;
@property (nonatomic, copy, readonly) NSString *link;

@end
