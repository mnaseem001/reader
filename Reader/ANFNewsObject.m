//
//  ANFNewsObject.m
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import "ANFNewsObject.h"

@implementation ANFNewsObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title":@"title",
             @"content":@"content",
             @"contentSnippet":@"contentSnippet",
             @"imageLink":@"imageLink",
             @"link":@"link"
    };
}

@end
