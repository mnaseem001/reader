//
//  ANFDailyNewsTableViewCell.h
//  Reader
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANFDailyNewsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIWebView *newsStorySnippetWebView;
@property (nonatomic, weak) IBOutlet UILabel *newsStoryTitleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *newsStoryImageView;

@end
