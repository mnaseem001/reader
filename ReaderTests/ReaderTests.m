//
//  ReaderTests.m
//  ReaderTests
//
//  Created by Mansoor Naseem on 8/10/15.
//  Copyright (c) 2015 AbercrombieAndFitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ANFDataController.h"
#import "ANFNewsObject.h"

@interface ReaderTests : XCTestCase

@end

@implementation ReaderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.

    XCTAssertNotNil([ANFDataController sharedInstance], @"Checking Singleton ANFDataController");
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Handler called"];
    [[ANFDataController sharedInstance] fetchDailyNewsListWithCompletionHandler:^(NSArray *dailyNewsList, NSError *error) {
        
        [expectation fulfill];
        if (!error) {

            XCTAssertEqual([dailyNewsList count], 10, @"Checking service returned exactly 10 news items");
            for (ANFNewsObject *newsObject in dailyNewsList)
            {
                XCTAssertNotNil(newsObject);
                XCTAssert([newsObject.title isKindOfClass:[NSString class]], @"Checking title is String");
                XCTAssert([newsObject.content isKindOfClass:[NSString class]],@"Checking content is String");
                XCTAssert([newsObject.contentSnippet isKindOfClass:[NSString class]], @"Checking contentSnippet is String");
            }
        }
        else
        {
            //First time there is no list.  caches it going forward.
            if (dailyNewsList != nil)
            {
                XCTAssertEqual([dailyNewsList count], 10, @"Checking service returned exactly 10 news items");
                for (ANFNewsObject *newsObject in dailyNewsList)
                {
                    XCTAssertNotNil(newsObject);
                    XCTAssert([newsObject.title isKindOfClass:[NSString class]], @"Checking title is String");
                    XCTAssert([newsObject.content isKindOfClass:[NSString class]],@"Checking content is String");
                    XCTAssert([newsObject.contentSnippet isKindOfClass:[NSString class]], @"Checking contentSnippet is String");
                }
            }
        }
        
    }];
    
    [self waitForExpectationsWithTimeout:1.0 handler:nil];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
