# README #

* Author: Mansoor Naseem
* Email: mnaseem001@yahoo.com
* Licence: None

### What is this repository for? ###

* RSS Feed Reader iOS App
* Version 0.1
* A news app based on the Google News RSS feed (http://news.google.com/?output=rss).

### How do I get set up? ###

* Download >> 
*     git clone https://mnaseem001@bitbucket.org/mnaseem001/reader.git
* Install >>
*     cd reader
*     pod install
*     open Reader.xcworkspace
* Requires:  Mac OSX 10.10, Xcode 6.4, iOS 8.4
* Run Tests on Xcode (Tests validity of the data fetched from service or locally)